import ddf.minim.analysis.*; //Importamos las librerias
import ddf.minim.*;
 
Minim minim; //definimos las distintas variables de las librerias
AudioPlayer player;
FFT fft; 
int[][] colo=new int[300][3];


void setup(){
  size(200, 512); //definimos el tamaño del lienzo
  noCursor(); //No dejamos que aparezca el cursor en el programa para no entorpecer la experiencia
  minim = new Minim(this); 
  player = minim.loadFile("polen.mp3",512); //asignamos al 'reproductor' la cancion que queremos al iniciar
  fft = new FFT(player.bufferSize(), player.sampleRate());
  player.play();
  surface.setResizable(true);
}

void draw(){
  background(0);
  stroke(255,50,100);
  //strokeWeight(0.75);

  fft.forward(player.mix);

 for(int i = 0; i < fft.specSize(); i++){
      fill(255,50,100);
      ellipse(i, 256 ,5 , fft.getBand(i)*45);
    }
    
    if ( player.isPlaying() )
  {
   text("Espacio para pausar \ny cambiar de canción.", 50, 480);
  }
  else
  {
   text("1 para cancion\n2 Para frecuencia 261Hz \n3 Para frecuencia 294Hz \n4 Para frecuencia 330Hz \n5 Para frecuencia 349Hz \n6 Para frecuencia 392Hz \n7 Para frecuencia 440Hz \n8 Para frecuencia 494Hz\nEspacio para reproducir.", 40, 350);
  }
}

void keyPressed()
{
  if(keyPressed && player.isPlaying()){
    
  }
  else
  {
    if(key == '1'){
      player = minim.loadFile("polen.mp3",512);
    }
    if(key == '2'){
      player = minim.loadFile("do261.mp3",512);
    }
    if(key == '3'){
      player = minim.loadFile("re294.mp3",512);
    }
    if(key == '4'){
      player = minim.loadFile("mi330.mp3",512);
    }
    if(key == '5'){
      player = minim.loadFile("fa349.mp3",512);
    }
    if(key == '6'){
      player = minim.loadFile("sol392.mp3",512);
    }
    if(key == '7'){
      player = minim.loadFile("la440.mp3",512);
    }
    if(key == '8'){
      player = minim.loadFile("si494.mp3",512);
    }
  }
  if (key == ' ' && player.isPlaying() )
  {
   player.pause();
  }
  else if (key == ' ')
  {
    player.play();
  }
}
